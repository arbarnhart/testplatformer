﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audiomufflescript : MonoBehaviour
  {
  public GameObject camerapos; // the player will die, so we do this and find it with tags.
  AudioLowPassFilter alpf;

  public float CloseCutOff = 21931f;
  public float FarCutOff = 952f;
  public float CloseDistance = 12f;
  public float FarDistance = 50f;


  void Start()
    {
    alpf = GetComponent<AudioLowPassFilter>();
    }

  // Update is called once per frame
  void Update()
    {
    if (camerapos == null)
      {
      camerapos = GameObject.FindGameObjectWithTag("MainFPSCam");
      }

    float distance = Vector3.Distance(transform.position, camerapos.transform.position);
    if (distance <= CloseDistance)
      {
      //we are close to audio so we make it clear
      alpf.cutoffFrequency = CloseCutOff;
      }
    else
      {
      if (distance >= FarDistance)
        {
        //we are far so we use max muffle
        alpf.cutoffFrequency = FarCutOff;
        }
      else
        {
        //we are in between close and far, so we apply a percentage of the muffle cut off
        float percent = (distance - CloseDistance) / (FarDistance - CloseDistance);

        //now apply the percentage to the difference between the close and far cut off values
        alpf.cutoffFrequency = ((FarCutOff - CloseCutOff) * percent) + CloseCutOff;
        }
      }
    }
  }
