﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
  public void MusicVolumeChange(float newValue)
    {
    MusicAndSFX.AudioImplementation.MusicVolumeChanged(newValue);
    }
  public void GameSoundsVolumeChange(float newValue)
    {
    MusicAndSFX.AudioImplementation.SFXVolumeChanged(newValue);
    }
  public void OnButton()
    {
    SceneManager.LoadScene("SampleScene");
    }

  // Update is called once per frame
  void Update()
    {
        
    }
}
