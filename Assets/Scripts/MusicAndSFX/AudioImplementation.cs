﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MusicAndSFX
  {
  // this is most of the implementation of the AudioManager
  // it's in a standard C# class 
  public class AudioImplementation : Object
    {

    // class is intended to be a singletone. default static constructor will create a single instance
    private static AudioImplementation _self = new AudioImplementation();
    public static AudioImplementation Singleton()
			{
      return _self;
			}

    // AudioSource for playing music in scenes
    AudioSource m_MyAudioSource;
    // A scene may have SFX on entering before music plays (optional)
    AudioClip clipIntro;
    // clip to play in current scene
    AudioClip clipLevelMusic;


    // Start is called before the first frame update - even though this isn't MonoBehavior derived, 
    //    it simplifies things to make calls match up. Note the gameObject 
    public void Start(GameObject gameObject)
      {
      // set folder locations based on namespace
      // set up call back for scene changes
      SceneManager.activeSceneChanged += ChangedActiveScene;
      // create AudioSource for scene music
      m_MyAudioSource = gameObject.AddComponent<AudioSource>();
      // ask Unity to please not destroy my AudioSource
      DontDestroyOnLoad(m_MyAudioSource);
      // set its volume to option settings
      m_MyAudioSource.volume = AudioManager.Instance.MusicVolume;
      // get the active (probably first) scene
      string sCurrentScene = SceneManager.GetActiveScene().name;
      // and play its music
      PlayMusicForScene(sCurrentScene);
      }

    // called by Unity when scene changes
    private void ChangedActiveScene(Scene current, Scene next)
      {
      // get the active scene
      string sCurrentScene = SceneManager.GetActiveScene().name;
      // and play its music
      PlayMusicForScene(sCurrentScene);
      }

    public void PlayMusicForScene( string sceneName)
			{
      bool bFound = false;
      foreach(AudioManager.SceneInfo sceneInfo in AudioManager.Instance.SceneOverRides)
				{
        if(sceneName == sceneInfo.SceneName)
					{
          bFound = true;
          clipIntro = sceneInfo.SceneIntroSound;
          clipLevelMusic = sceneInfo.SceneMusic;
          break;
					}
				}
      if( !bFound)
				{
        clipIntro = AudioManager.Instance.DefaultIntroSound;
        clipLevelMusic = AudioManager.Instance.DefaultSceneMusic;
				}
      if (clipIntro != null)
        {
        m_MyAudioSource.clip = clipIntro;
        m_MyAudioSource.loop = false;
        m_MyAudioSource.Play();
        }
      else if (clipLevelMusic != null)
        {
        m_MyAudioSource.clip = clipLevelMusic;
        m_MyAudioSource.loop = true;
        m_MyAudioSource.Play();
        }
      else
        m_MyAudioSource.Stop();

      }
    public static void PlayClipAtPoint(string sClip, Vector3 pt)
			{
      AudioClip clip = FindClip(sClip);
      if (clip != null)
        AudioSource.PlayClipAtPoint(clip, pt, AudioManager.Instance.GamePlayVolume);
      else
				{
        
				}
			}
    public static AudioClip FindClip(string sfxName)
      {
      AudioClip audioClip = null;
      foreach (AudioManager.ClipsByCategoryClass clips in AudioManager.Instance.ClipsByCategory)
        {
        foreach (AudioClip clip in clips.Clips)
          {
          if (clip.name == sfxName)
            return clip;
          }
        }
      return audioClip;
      }
    public static AudioManager.RumblingSound FindRumbleClip(string sfxName)
      {
      foreach (AudioManager.RumblingSound clip in AudioManager.Instance.RumblingSounds)
        {
          if (clip.SoundEffect.name == sfxName)
            return clip;
        }
      return null;
      }
    public static void MusicVolumeChanged(float newValue = -1.0f)
      {
      if (newValue >= 0.0f)
        AudioManager.Instance.MusicVolume = newValue;

      if (_self.m_MyAudioSource != null)
        _self.m_MyAudioSource.volume = AudioManager.Instance.MusicVolume;
      }

    public static void SFXVolumeChanged(float newValue = -1.0f)
      {
      if (newValue >= 0.0f)
        AudioManager.Instance.GamePlayVolume = newValue;
      }

    // Update is called once per frame
    public void Update()
      {
      // if it has stopped, opening music is done
      if (!m_MyAudioSource.isPlaying && clipLevelMusic != null)
        {
        m_MyAudioSource.clip = clipLevelMusic;
        m_MyAudioSource.loop = true;
        m_MyAudioSource.Play();
        }
      int iWCs = listWiredComponents.Count;
      while (iWCs > 0)
        {
        iWCs--;
        WiredComponent oWC = listWiredComponents[iWCs];
        if (oWC.Validate())
          oWC.Update();
        else
          listWiredComponents.RemoveAt(iWCs);
        }

      }
    static List<WiredComponent> listWiredComponents = new List<WiredComponent>();
    public static WiredComponent AddWiredComponent(GameObject GO, string sSound, WiredSoundType WST)
      {
      WiredComponent oWC = new WiredComponent(GO, sSound, WST);
      listWiredComponents.Add(oWC);
      return oWC;
      }
    }
  }