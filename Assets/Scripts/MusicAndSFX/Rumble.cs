﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace MusicAndSFX
	{
	// class Rumble is for vibrating gamepads
	//  note that it only has static calls
	public class Rumble
		{
		// object for gamepad, if there is one
		private static object oController = null;
		// method to invoke on object
		private static MethodInfo methodSetMotorSpeeds = null;
		// make sure we have called Init;
		private static bool bInit = false;
		// boolean to note we found InputSystem, defaults to true as we only want to set false once and check for that
		private static bool bInputSystem = true;
		// time we last checked for a gamepad. Start with -2 seconds so we will check right away
		private static float timeLastCheckedForGamePad = -2.0f;

		// InitRumble function instead of static constructor we only bother with it if SetMotorSpeeds is ever called
		private static void InitRumble()
			{
			// if we already checked for InputSystem and didn't find it, that won't change
			if (!bInputSystem)
				return;
			// check to see if we have initialized
			if (bInit && oController != null)
				return;
			// note that InitRumble has been called
			bInit = true;
			// I nearly always use try/catch around Reflection
			//   in this case, the processing is optional anyway
			try
				{
				// Basically a test to see if InputSystem is being used in this game
				Type typeGamepad = null;
				// if it is used, we will attempt to get the controller and the method for setting mtor speeds

				Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
				foreach(Assembly assembly in assemblies)
					{
					typeGamepad = assembly.GetType("UnityEngine.InputSystem.Gamepad");
					if (typeGamepad != null)
						break;
					}
				bInputSystem = (typeGamepad != null);
				if (bInputSystem)
					{
					// note that we found InputSystem - if we don't find a current controller, we still want to check at intervals
					bInputSystem = true;
					// note time we are testing
					timeLastCheckedForGamePad = Time.time;
					// try to get current gamepad
					oController = typeGamepad.GetProperty("current", BindingFlags.Static | BindingFlags.Public).GetValue(null, null);
					if (oController != null && methodSetMotorSpeeds == null)
						{
						// not really dependent on havingfound a controller, but pointless to do it if not
						Type[] parameterTypes = new Type[] { typeof(float), typeof(float) };
						// let's get ready to rumble! get the method call for setting motor speeds
						methodSetMotorSpeeds = typeGamepad.GetMethod("SetMotorSpeeds", parameterTypes);
						}
					}
				}
			catch(Exception ex)
				{
				Debug.LogError("Exception in Rumble.Init " + ex.Message);
				}
			}
		public static void SetMotorSpeeds(float lowFrequency, float highFrequency)
			{
			// make sure we have initialized; will short circuit if no gamepad
			InitRumble();
			try
				{
				// see if we have a controller and the method to call
				if (oController != null && methodSetMotorSpeeds != null)
					{
					// call the gamepad's motor speed function
					object[] parameters = new object[] { lowFrequency, highFrequency };
					methodSetMotorSpeeds.Invoke(oController, parameters);
					}
				}
			catch (Exception ex)
				{
				Debug.LogError("Exception in Rumble.SetMotorSpeeds " + ex.Message);
				}
			}
		}
	}
