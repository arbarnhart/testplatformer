﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MusicAndSFX
  { 
  public enum WiredSoundType
    {
    LoopWhileMoving,
    OneShotWhenMoved,
    SelfManaged
    }
  public class WiredComponent : Object
    {
    public GameObject gameObject;
    public AudioSource audioSource;
    public WiredSoundType wiredSoundType;
    private Vector3 vLastPos;
    private bool bFired = false;
    public float maxDistance = 50.0f;
    public float lowFreqRumble = 0.0f;
    public float highFreqRumble = 0.0f;
    public float rumbleSeconds = 0.0f;

    public WiredComponent(GameObject GO, AudioSource AS, WiredSoundType WST)
      {
      gameObject = GO;
      audioSource = AS;
      wiredSoundType = WST;
      vLastPos = audioSource.transform.position;
      }
    public WiredComponent(GameObject GO, string sSound, WiredSoundType WST)
      {
      gameObject = GO;
      audioSource = gameObject.AddComponent<AudioSource>();
      audioSource.volume = AudioManager.Instance.GamePlayVolume;
      audioSource.clip = AudioImplementation.FindClip(sSound);
      wiredSoundType = WST;
      vLastPos = audioSource.transform.position;
      }
    public bool Validate()
			{
      if (gameObject == null)
        return false;
      if (audioSource == null)
        return false;
      return true;
			}
    public void Update()
      {
      if (wiredSoundType == WiredSoundType.OneShotWhenMoved)
        {
        OneShotMovedUpdate();
        }
      else if (wiredSoundType == WiredSoundType.LoopWhileMoving)
        {
        LoopWhileMovingUpdate();
        }
      }
    private void OneShotMovedUpdate()
      {
      if (!bFired)
        {
        float distance = Vector3.Distance(audioSource.transform.position, vLastPos);
        if (distance > 0.001)
          {
          bFired = true;
          audioSource.Play();
          }
        }
      }
    private void LoopWhileMovingUpdate()
      {
      float distance = Vector3.Distance(audioSource.transform.position, vLastPos);
      if (distance > 0.001)
        {
        vLastPos = audioSource.transform.position;
        if (!audioSource.isPlaying)
          {
            {
            audioSource.loop = false;
            audioSource.Play();
            }
          }
        }
      else
        {
        if (audioSource.isPlaying)
          {
            {
            audioSource.Stop();
            audioSource.loop = false;
            }
          }
        }
      }
    }
  }
