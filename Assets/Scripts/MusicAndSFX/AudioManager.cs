﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MusicAndSFX
  {
  public class AudioManager : MonoBehaviour
    {
    //public properties for inspector
    //-------------------------------
    [Range(0f, 1f)] 
    public float MusicVolume = 1f;
    [Range(0f, 1f)]
    public float GamePlayVolume = 1f;
    // music to play if no specific music set for the scene
    public AudioClip DefaultSceneMusic;
    // intro sound to play when scene starts if there is no override
    public AudioClip DefaultIntroSound;
    // any overrids - scenes that use different music and intro (or none)
    public List<SceneInfo> SceneOverRides;
    // named resource - just makes it easier to refer to sounds or music by a label and switch out what actual sound plays
    public List<ClipsByCategoryClass> ClipsByCategory;
    // rumbling sounds that use controller vibration
    public List<RumblingSound> RumblingSounds;

    // Singleton implementation for this object
    private static AudioManager instance = null;
    public static AudioManager Instance
      {
      get
        {
        if (instance == null)
          {
          instance = (AudioManager)FindObjectOfType(typeof(AudioManager));
          }
        return instance;
        }
      }
    void Awake()
      {
      if (Instance != this)
        {
        Destroy(gameObject);
        }
      else
        {
        DontDestroyOnLoad(gameObject);
        }
      }


    // get the singleton instance of the implementation class
    public static AudioImplementation Implementation = AudioImplementation.Singleton();

    // Start is called before the first frame update
    void Start()
      {
      try
        {
        // everything really done in implementation class
        Implementation.Start(gameObject);
        Rumble.SetMotorSpeeds(0.5f, 0.5f);
        }
      catch (System.Exception ex)
        {
        Debug.LogException(ex);
        };
      }

    static bool bMotorOff = false;
    // Update is called once per frame
    void Update()
      {
      try
        {
        if (!bMotorOff && Time.realtimeSinceStartup > 3.0)
          {
          Rumble.SetMotorSpeeds(0f, 0f);
          bMotorOff = true;
          }
        Implementation.Update();
        }
      catch (System.Exception ex)
        {
        Debug.LogException(ex);
        };
      }
    [System.Serializable]
    public class ActivityOrMoodMusicClass
      {
      public string ActivityOrMood;
      public string Music;
      }
    [System.Serializable]
    public class ClipsByCategoryClass
      {
      public string CategoryName;
      public List<AudioClip> Clips;
      }
    [System.Serializable]
    public class SceneInfo
      {
      public string SceneName;
      public AudioClip SceneIntroSound;
      public AudioClip SceneMusic;
      }
    [System.Serializable]
    public class RumblingSound
      {
      public AudioClip SoundEffect;
      [Range(0f, 1f)]
      public float LowFrequency;
      [Range(0f, 1f)]
      public float HighFrequency;
      }
    }
  }