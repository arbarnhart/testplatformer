﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MusicAndSFX
  {
  public class SceneMusic : MonoBehaviour
    {
    // Start is called before the first frame update
    public List<MultiTrackClass> Songs;

    private List<PooledAudioSource> audioSourcePool = new List<PooledAudioSource>();
    internal static SceneMusic Instance = null;
    internal MultiTrackClass Song = null;
    internal MultiTrackClass FadingSong = null;

    void Awake()
      {
      Instance = this;
      }
    void Start()
      {
      if (Songs.Count > 0)
        {
        Song = Songs[0];
        Song.Play();
        }
      }

    // Update is called once per frame
    void Update()
      {
      if (Song != null)
        Song.Update();
      }
    public void SetPartVolume(string sMood, float fNewVolume)
      {

      }
    internal PooledAudioSource GetAudioSource()
      {
      PooledAudioSource pooledAudioSource;
      int iSources = audioSourcePool.Count;
      while (iSources > 0)
        {
        iSources--;
        pooledAudioSource = audioSourcePool[iSources];
        if (pooledAudioSource == null)
          audioSourcePool.RemoveAt(iSources);
        else if (!pooledAudioSource.inUse)
          {
          pooledAudioSource.inUse = true;
          return pooledAudioSource;
          }
        }
      pooledAudioSource = new PooledAudioSource();
      pooledAudioSource.audioSource = gameObject.AddComponent<AudioSource>();
      pooledAudioSource.inUse = true;
      return pooledAudioSource;
      }
    }
  internal class PooledAudioSource
		{
    internal AudioSource audioSource = null;
    internal bool inUse = false;
		}

  [System.Serializable]
  public class MultiTrackClass
    {
    public string Name;
    [Range(0f, 1f)]
    public float OverallVolume = 1.0f;
    public List<PartialTrackClass> Parts;
    internal void Play()
      {
      foreach (PartialTrackClass part in Parts)
        {
        part.pooledAudioSource = SceneMusic.Instance.GetAudioSource();
        part.pooledAudioSource.audioSource.clip = part.Clip;
        part.pooledAudioSource.audioSource.volume = part.CurrentVolume * OverallVolume * AudioManager.Instance.MusicVolume;
        part.pooledAudioSource.audioSource.loop = true;
        }
      double startTime = AudioSettings.dspTime + 1.0f;
      // start them playing after all are created to make timing simpler
      foreach (PartialTrackClass part in Parts)
        part.pooledAudioSource.audioSource.PlayScheduled(startTime);
      }

    // Update is called once per frame
    internal void Update()
      {
      foreach (PartialTrackClass part in Parts)
        {
        if (part.CurrentVolume != part.TargetVolume)
          {
          if (part.CurrentVolume < part.TargetVolume)
            {
            part.CurrentVolume += part.StepAdjustVolume;
            if (part.CurrentVolume > part.TargetVolume)
              part.CurrentVolume = part.TargetVolume;
            }
          else
            {
            part.CurrentVolume -= part.StepAdjustVolume;
            if (part.CurrentVolume < part.TargetVolume)
              part.CurrentVolume = part.TargetVolume;
            }
          part.pooledAudioSource.audioSource.volume = part.CurrentVolume * OverallVolume * AudioManager.Instance.MusicVolume;
          }
        }
      }
    public void SetPartVolume(string sClip, float fNewVolume)
      {

      }
    }
  [System.Serializable]
  public class PartialTrackClass
    {
    public string Mood;
    public AudioClip Clip;
    [Range(0f, 1f)]
    public float CurrentVolume = 1f;
    [Range(0f, 1f)]
    public float TargetVolume = 1f;
    [Range(0.001f, 1f)]
    public float StepAdjustVolume = 0.01f;
    [Range(0.5f, 1.5f)]
    internal PooledAudioSource pooledAudioSource;
    }
  }
